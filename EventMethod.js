/**
* @fileoverview イベント用に作成した関数。
*/
/**
* 書式を指定して文字数などをステータスバーに表示する。<br />
* 以下の規則に従って書式中の「{}」で囲まれた部分を置換する。
* <table>
* <tr><th></th><th>論理行（アクティブ）</th><th>論理行（アンカー）</th><th>表示行（アクティブ）</th><th>表示行（アンカー）</th></tr>
* <tr><th>行数</th><td>lo</td><td>lo</td><td>vo</td><td>vo</td></tr>
* <tr><th>X座標=桁</th><td>lcx</td><td>lnx</td><td>vcx</td><td>vnx</td></tr>
* <tr><th>Y座標=行</th><td>lcy</td><td>lny</td><td>vcy</td><td>vny</td></tr>
* <tr><th>文字数</th><td>lcl</td><td>lnl</td><td>vcl</td><td>vnl</td></tr>
* </table>
* <ul>
* <li>sc:アクティブポイントの文頭からの位置</li>
* <li>sn:アンカーポイントの文頭からの位置</li>
* <li>sl:文書の文字数</li>
* <li>ss:選択文字数</li>
* </ul>
* @param {string} seg 非選択時の書式
* @param {string} selSeg 選択時の書式
*/
Event.TextCount=function(seg,selSeg){
	var $d=Editor.ActiveDocument;
	var $s=$d.Selection;
	var template=($s.IsEmpty?seg?seg:"{sc}/{sl}字 L:{lcy}/{lo}行 {lcx}/{lcl}桁 V:{vcy}/{vo}行 {vcx}/{vcl}桁":selSeg?selSeg:"{sn}"+($s.GetActivePos()>$s.GetAnchorPos()?" + ":" - ")+"{ss}={sc}");
	Status=template.replace(/{(?:[lv](?:[cn][xyl]|o)|s[cnls])}/g,function($1){
		switch($1){
			case "{lcx}":return $s.GetActivePointX(mePosLogical);
			case "{lcy}":return $s.GetActivePointY(mePosLogical);
			case "{lnx}":return $s.GetAnchorPointX(mePosLogical);
			case "{lny}":return $s.GetAnchorPointY(mePosLogical);
			case "{lo}":return $d.GetLines(0);
			case "{lcl}":return $d.GetLine($s.GetActivePointY(mePosLogical),0).length+1;
			case "{lnl}":return $d.GetLine($s.GetAnchorPointY(mePosLogical),0).length+1;
			case "{vcx}":return $s.GetActivePointX(mePosView);
			case "{vcy}":return $s.GetActivePointY(mePosView);
			case "{vnx}":return $s.GetAnchorPointX(mePosView);
			case "{vny}":return $s.GetAnchorPointY(mePosView);
			case "{vo}":return $d.GetLines(meGetLineView);
			case "{vcl}":return $d.GetLine($s.GetActivePointY(mePosView),meGetLineView).length+1;
			case "{vnl}":return $d.GetLine($s.GetAnchorPointY(mePosView),meGetLineView).length+1;
			case "{sc}":return $s.GetActivePos();
			case "{sn}":return $s.GetAnchorPos();
			case "{sl}":return $d.Text.length;
			case "{ss}":return $s.Text.length;
			default:return "";
		}
	});
}
/**
* 直前のアクティブタブを保存・取得
* @return:<string> 直前のアクティブタブ。
*/
Event.SaveActiveTab=function(){
	var fso=new ActiveXObject("Scripting.FileSystemObject");
	var folder=fso.BuildPath(fso.GetParentFolderName(Editor.FullName),"store");
	var result="";
	var arr=[$d=Editor.ActiveDocument.FullName];
	if(!fso.FolderExists(folder)){
		fso.CreateFolder(folder);
	}
	var file=fso.OpenTextFile(fso.BuildPath(folder,"tabstate"),1,1,-1);
	if(!file.AtEndOfStream){
		arr[1]=file.ReadAll().split("\r\n")[0];
		result=arr[1];
	}
	file.Close();
	file=fso.OpenTextFile(fso.BuildPath(folder,"tabstate"),2,1,-1);
	file.Write(arr.join("\r\n"));
	file.Close();
	return result;
}
/**
* パターンにマッチしたファイルを保存する。
* パターンはEvent.AutoSave.patternに登録する。
*/
Event.AutoSave=function(){
	var $d=Editor.ActiveDocument;
	if(new RegExp(Event.AutoSave.pattern.join("|")).test($d.FullName)){
		$d.Save();
	}
}
/**
* ファイル名のマッチパターン。<br />
* 正規表現用のエスケープが必要。
* @static
*/
Event.AutoSave.pattern=[];
