#title="イベント本体"
#include "Event.js"
#include "EventMethod.js"
#include "..\AutoMemo.js"

/**
* @fileoverview イベント本体。<br />
* イベントマクロとしてMeryに登録するファイルではこのファイルをインクルードしてEvent.executeを実行するだけにする。具体的な処理はこのファイルの関数内に記述する。
*/

//カーソルが移動した時
function OnCaretMoved(){
	Event.TextCount();
}
//テキストが変更された時
function OnChanged(){}
//文字が挿入された時
function OnChar(){}
//文書を閉じた時
function OnDocumentClose(){
}
//アクティブな文書が変更された時
function OnDocumentSelectChanged(){
}
//ファイルを開いた時
function OnFileOpened(){
}
//ファイルを保存した時
function OnFileSaved(){}
//ファイルを保存する前
function OnFileSaving(){}
//フォーカスを受け取った時
function OnFocus(){}
//アイドル状態になった時
function OnIdle(){}
//フォーカスを失った時
function OnKillFocus(){}
//編集モードが変更された時
function OnModeChanged(){}
//更新状態が変更された時
function OnModified(){
}
//スクロールした時
function OnScroll(){}
//選択範囲が変更された時
function OnSelChanged(){}
//タブを移動した時
function OnTabMoved(){}
