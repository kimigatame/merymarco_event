#title="イベント用関数"

/**
* @fileoverview イベント用クラス。
*/

/**
* イベント用クラス。
* @class
*/
var Event=function(){};
/**
* 例外処理込みでイベントを実行する。<br />
* イベント名と同名の関数が呼び出される。イベントの処理はその中に記述する。
* @param {string} eventName イベント名
*/
Event.execute=function(eventName){
	var event;
	switch(eventName){
		case "OnCaretMoved":event=OnCaretMoved;break;
		case "OnChanged":event=OnChanged;break;
		case "OnChar":event=OnChar;break;
		case "OnDocumentClose":event=OnDocumentClose;break;
		case "OnDocumentSelectChanged":event=OnDocumentSelectChanged;break;
		case "OnFileOpened":event=OnFileOpened;break;
		case "OnFileSaved":event=OnFileSaved;break;
		case "OnFileSaving":event=OnFileSaving;break;
		case "OnFocus":event=OnFocus;break;
		case "OnIdle":event=OnIdle;break;
		case "OnKillFocus":event=OnKillFocus;break;
		case "OnModeChanged":event=OnModeChanged;break;
		case "OnModified":event=OnModified;break;
		case "OnScroll":event=OnScroll;break;
		case "OnSelChanged":event=OnSelChanged;break;
		case "OnTabMoved":event=OnTabMoved;break;
		default:return;
	}
	try{
		event();
	}
	catch(e){
		OutputBar.Visible=true;
		OutputBar.Writeln("Event Error : "+(eventName||"")+" : "+e.description);
	}
}
