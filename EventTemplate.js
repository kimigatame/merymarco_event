/**
* イベントを考慮したマクロテンプレート。<br />
* このテンプレートに沿うことで、Meryからの呼び出し、イベントでのインクルード両方に1ファイルで対応できる。<br />
* 必要なファイル。
* <ol style="list-style-type:upper-roman;">
* <li>このテンプレートを利用したマクロファイル。</li>
* <li>Event.js。</li>
* </ol>
* Meryから直接呼び出す場合、「Event==null」のブロック内に処理を記述する。<br />
* イベントで利用する場合は、Eventクラスを導入しておく。必要な処理は関数として記述する（読み込んですぐ実行されてはいけない）。<br />
* インクルードは必ずEvent.jsより後で行う。
*/
var Event;
if(Event==undefined){
	//直接実行される場合の処理
	template();
}

/**
* イベントで実行する処理。
*/
function template(){}
